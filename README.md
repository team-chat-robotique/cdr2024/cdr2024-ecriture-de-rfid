# CDR2024 - Écriture des tags RFID des plantes

Ce code sert à écrire des valeurs sur des tags `NTAG213` ou `NTAG215` pour la [Coupe de France de Robotique 2024](https://www.coupederobotique.fr/edition-2024/le-concours/reglement-2024).

## Branchements

| Module RC522 | Arduino Uno/Nano | Arduino Mega |
| ------------ | ---------------- | ------------ |
| SDA          | D10              | D9           |
| SCK          | D13              | D52          |
| MOSI         | D11              | D51          |
| MISO         | D12              | D50          |
| IRQ          | N/A              | N/A          |
| GND          | GND              | GND          |
| RST          | D9               | D8           |
| 3.3V         | 3.3V             | 3.3V         |

| Bouton | Pin (dans le code) |
| ------ | ------------------ |
| `BTN1` | `5`                |
| `BTN2` | `6`                |

## Boutons

Les pins des boutons sont configurés en `INPUT_PULLUP`, ce qui veut dire que les boutons doivent être branchés entre les pins et la masse pour que leur état soit détecté :

![Schéma du circuit](docs/schematic.png)

## Références

* [Règlement](https://www.coupederobotique.fr/wp-content/uploads/Eurobot2024_Rules_CUP_FR.pdf)
* [Arduino - Input PullUp](https://docs.arduino.cc/built-in-examples/digital/InputPullupSerial)



