#include <Arduino.h>
#include <MFRC522.h>
#include <SPI.h>

//
// CONSTANTES
//
// Boutons
#define BTN1 5
#define BTN2 6

// RC522 : Pins d'Arduino Uno / Nano
#define RST_PIN 9
#define SS_PIN 10

// RFID Block
#define BLOCK_ADDR 4

// FLAGS
int lastBTN1state = HIGH;
int lastBTN2state = HIGH;
int BTN1state = HIGH;
int BTN2state = HIGH;

//
// INSTANCES
//
MFRC522 mfrc522(SS_PIN, RST_PIN);

//
// PROTOTYPES
//
char RFID_read(void);
void RFID_write(char);
bool initCard(void);

void setup() {
    Serial.begin(9600);
    SPI.begin();
    mfrc522.PCD_Init();
    delay(4);

    pinMode(BTN1, INPUT_PULLUP);
    pinMode(BTN2, INPUT_PULLUP);
}

void loop() {
    // Détection de carte
    while (!initCard())
        ;

    // La carte vient d'être détectée
    Serial.println("======================");
    Serial.println("Nouveau RFID détecté !");
    Serial.print("Tag sur le RFID :");
    Serial.println(RFID_read());

    while (true) {
        BTN1state = digitalRead(BTN1);
        BTN2state = digitalRead(BTN2);
        if ((BTN1state == LOW) && (BTN1state != lastBTN1state)) {
            // Plante fragile
            Serial.println("Plante fragile (blanc)");
            RFID_write('1');
            Serial.println("RFID encodé avec le tag : 1");
            Serial.println("Succès !");
            lastBTN1state = BTN1state;
            delay(50);
            break;
        } else if ((BTN2state == LOW) && (BTN2state != lastBTN2state)) {
            // Plante résistance
            Serial.println("Plante résistante (violet)");
            RFID_write('2');
            Serial.println("RFID encodé avec le tag : 2");
            Serial.println("Succès !");
            lastBTN2state = BTN2state;
            delay(50);
            break;
        }
    }

    // Halt PICC
    mfrc522.PICC_HaltA();
    // Stop encryption on PCD
    mfrc522.PCD_StopCrypto1();
}

bool initCard() {
    if (!mfrc522.PICC_IsNewCardPresent()) {
        // Aucune carte détectée
        return false;
    }

    if (!mfrc522.PICC_ReadCardSerial()) {
        // Le UID de la carte n'est pas correct
        return false;
    }
    return true;
}

char RFID_read() {
    int blockAddr = BLOCK_ADDR;
    byte readData[18];
    byte size = 18;
    char readChar;

    MFRC522::StatusCode status = mfrc522.MIFARE_Read(blockAddr, readData, &size);

    if (status == MFRC522::StatusCode::STATUS_OK) {
        readChar = (char)readData[0];
        mfrc522.PICC_HaltA();
    }
    return readChar;
}

void RFID_write(char charToWrite) {
    byte data = charToWrite;
    byte blockAddr = BLOCK_ADDR;

    MFRC522::StatusCode status = mfrc522.MIFARE_Ultralight_Write(blockAddr, &data, 4);
    if (status == MFRC522::StatusCode::STATUS_OK) {
        mfrc522.PICC_HaltA();
    }
}
